<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *

     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('referer_id')->required();
            $table->bigInteger('referred_id')->required();
            $table->bigInteger('listing_id')->required();
            $table->bigInteger('maturity_days')->required();
            $table->bigInteger('bonus_amount')->required();
            $table->bigInteger('bonus_percentage')->required();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuses');
    }
}
