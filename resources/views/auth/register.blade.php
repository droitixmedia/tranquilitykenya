@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- register begin -->
            <div class="register">
                <div class="container">
                    <div class="reg-body">
                         <form action="{{ route('register') }}" method="POST" autocomplete="off" id="register_user" >
                                    @csrf
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <h4 class="sub-title">Create Account</h4>
                                    
                                       <div class="form-group">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" type="text" value="{{ old('name') }}" required autofocus  placeholder="Enter First Name">
                                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" type="text" value="{{ old('surname') }}" required autofocus  placeholder="Enter Last Name">
                                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" id="phone_number" type="text" value="{{ old('phone_number') }}" required autofocus  placeholder="Phone Number">
                                         @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    
                                    
                                   
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 additional-info">
                                 <h4 class="sub-title">Tranquility</h4>  
                                    <div class="form-group">
                                       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                   <div class="form-group">
                                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>  

                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">
                                    </div>
                                     
                                </div>   
                            </div>
                            
                            <div class="row">
                                <div class="col-xl-6 col-lg-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios5" value="option2">
                                        <label class="form-check-label" for="exampleRadios5">
                                            I agree to the terms &amp; conditions.
                                        </label>
                                        <p>(*) We will never share your personal & financial information with third parties.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6">
                                    <button class="def-btn btn-form" type="submit">Create Account <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- register end -->
     
        
        

@endsection
