@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>My Dashboard</h1>
                                <ul>
                                    <li>home</li>
                                    <li>Dashboard</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                                <div class="part-data">
                                    <span class="name">Hi, {{Auth::user()->name}}</span>
                                    <ul>
                                        <li>
              @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
                                       

                            @php ($sum += ($listing->amount)+(($multiplier )*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach

                                            <span class="text">Balance</span>
                                            <span class="number">Ksh{{$sum-$diff}}</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                                <div class="part-img">
                                    <img src="/uploads/avatars/{{Auth::user()->avatar}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="user-statics">
                        <div class="row">
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/icon/002-cash.png" alt="">
                                    </div>
                                    <div class="part-info">
                                          
                                   
                                                           
                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                            @php ($sum += ($listing->amount)+(($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach


                                        <span class="number">Ksh{{$sum-$diff}}</span>
                                        <span class="text">Current Balance</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('listings.published.index', [$area]) }}">
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/icon/003-credit-card.png" alt="">
                                    </div>
                                    <div class="part-info">
                                        @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                      
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
               
                      @endforeach
                                        <span class="number">Ksh {{$diff}}</span>
                                        <span class="text">Withdrawals</span>
                                    </div>
                                </div>
                            </a>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('listings.published.index', [$area]) }}">
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/icon/001-donation.png" alt="">
                                    </div>
                                    <div class="part-info">
                                        @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                   
                            @if($listing->matched())


                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif


                     @endforeach

                                        <span class="number">Ksh{{$sum}}</span>
                                        <span class="text">Deposits Total</span>
                                    </div>
                                </div>
                                </a>
                            </div>
                            
                            
                           
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                 <a href="{{ url('referrals') }}">
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/icon/007-referral.png" alt="">
                                    </div>
                                    <div class="part-info">
                                         @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                                        <span class="number">Ksh{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}</span>
                                        <span class="text">Total Bonuses</span>
                                    </div>
                                </div>
                            </a>
                            </div>
                            
                        </div>
                    </div>
         
                    <div class="refferal-info">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <div class="part-icon">

                                    </div>
                                    <div class="part-text">
                                        <span class="title">Your Referral Link</span>
                                        <span class="descr">{{$referralink}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <div class="part-icon">

                                    </div>
                                    <div class="part-text">
                                        <span class="title"><a href="{{url('referrals')}}" >Your Downliners</a></span>
                                        <span class="descr"><a href="{{url('referrals')}}" > <i class="fas fa-users"></i> {{ Auth::user()->referrals()->count()}}</a> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <div class="part-icon">

                                    </div>
                                    <div class="part-text">
                                        <span class="title">Downliner Investment Bonus</span>
                                        <span class="descr"> <i class="fas fa-coins"></i> Ksh{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')}}.00 | <a href="{{ route('listings.bcreate', [$area]) }}">Withdraw</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <div class="part-icon">
    
                                    </div>
                                    <div class="part-text">
                                        <span class="title">Total Bonus</span>
                                        <span class="descr"> <i class="fas fa-donate"></i>  Ksh{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')}}.00 | <a href="{{ route('listings.bcreate', [$area]) }}">Withdraw</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                                      <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="transactions-table">
                                <h3 class="title">
                                   Downliner Investment Bonus
                                </h3>
                                <div class="table-responsive">
                                    <table id="example" class="display misco-data-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Bonus Received</th>
                                                
                                                
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(openjobs\Bonus::where('referer_id', auth()->user()->id)->get() as $bonus)
                                            <tr>
                                                <td>{{$bonus->id}}. Recieved  Bonus from {{$bonus->name}} {{$bonus->surname}}</td>
                                             
                                                
  
                                                
                                            </tr>
                                              @endforeach
                                           
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                   
                </div>
            </div>
            <!-- account end -->

@endsection
