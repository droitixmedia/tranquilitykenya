@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>My Proof of Payments</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Proof</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="col-xl-7 col-lg-7 col-md-7">
                           
                            <div class="last-step">
                                <div>
                                 <table class="table mb-0 table-hover">
                                           <tbody>
                                     @if ($files->count())
        @each ('resumes.partials._file_own', $files, 'file')

    @else
        <tr class="mb30">
                                                <th scope="row">
                                                    <ul>
                                                        <li class="list-inline-item"><a href=""><i class="fe fe-document"></i></a></li>
                                                        <li class="list-inline-item cv_sbtitle">You have not uploaded any proof yet <u><a href="{{route('files.upload.index')}}">UPLOAD NOW</a></u> </li>
                                                    </ul>
                                                </th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>

                                                </td>
                                            </tr>

    @endif

                                        </tbody>
                                        </table>
                            
                        </div>
                    </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
