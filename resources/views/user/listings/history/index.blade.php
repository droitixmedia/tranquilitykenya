@extends('layouts.app')

@section('title', 'All Transactions')

@section('description')

@endsection

@section('content')


<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>

                            <li>
                                <a href="{{url('/')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                            </li>
                            @role('admin')
                            <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                              <li>
                                <a href="{{ url('admin/impersonate')}}"><i class="fe fe-lock"></i> <span>Impersonate</span></a>
                            </li>
                            @endrole
                            @if (session()->has('impersonate'))
                              <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                            @endif
                            @if (session()->has('impersonate'))
                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="fe fe-lock"></i> Stop Impersonating</a>
                        </li>
                        <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

              @endif

                            <li>
                                <a href="{{url('bidding')}}"><i class="fe fe-bell"></i> <span>Bidding Rooms</span></a>
                            </li>
                             <li>
                                <a href="{{ route('comments.published.index') }}"><i class="fe fe-document"></i> <span>My Bids</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fe fe-tiled"></i> <span>Banked Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.published.index', [$area]) }}"><i class="fe fe-money"></i> <span>Selling Coins</span></a>
                            </li>
                             <li  class="active">
                                <a href="{{ route('listings.history.index', [$area]) }}"><i class="fe fe-file"></i> <span>All Transactions</span></a>
                            </li>


                            <li>
                                <a href="{{ route('profile') }}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
                            </li>
                                <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fe fe-logout"></i>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Sidebar -->






<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Your History</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                                    <li class="breadcrumb-item active">History</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-table">
                                <div class="card-header">
                                    <h4 class="card-title">Coin History</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Reference</th>
                                                    <th>Amount</th>
                                                    <th>Selling Price</th>
                                                    <th>Bought</th>

                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                                  @if ($listings->count())
        @each ('listings.partials.listing_history', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">You Currently have no History</p></th>
    @endif


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <!-- /Page Wrapper -->







@endsection
