@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Investment Dashboard</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Investment Dashboard</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    
                     
                    
                            
                            <div class="row">
                              @if ($listings->count())
        @each ('listings.partials.listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">YOU HAVE NOT DEPOSITED ANYTHING YET</p></th>
    @endif

    </div>

                            </div>
                           
                        </div>
                            
                        </div>
                  


                   
                </div>
            </div>
            <!-- account end -->

@endsection
