@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                              @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach

                        <h4>K {{$listing->amount - $sum}}</h4>

                        <div class="card mb-5">
                            <div class="card-body">
                                @if(session('success'))
    <h1 style="color:green">{{session('success')}}</h1>
@endif
                    @if($listing->amount-$sum < 200)
                     <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                    <div class="form-group">
                                       @include('listings.partials.forms.categories')
                                    </div>

                                        <input type="hidden" class="form-control" name="body" id="body" value="{{$listing->amount-$sum}}">
                                         <input type="hidden" class="form-control" name="split" id="split" value="{{$listing->amount-$sum}}">

                                    <button type="submit" class="btn btn-primary pd-x-20">Bid All</button>
                                </div>
                        </form>



                    @else

                        <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                    <div class="form-group">
                                       @include('listings.partials.forms.categories')
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="body" id="body" value="{{$listing->amount-$sum}}">
                                                <div class="form-group{{ $errors->has('split') ? ' has-error' : '' }}">
                                            <label>Specify amount you can afford</label>
                                            <input type="text" name="split" class="form-control">

                                              @if ($errors->has('split'))
                                <span class="help-block">
                                    {{ $errors->first('split') }}
                                </span>
                            @endif
                                    </div>
                                    <button type="submit" class="btn btn-primary pd-x-20">Bid</button>
                                </div>
                        </form>

                        @endif

                            </div>
                        </div>
                        </div>
                        <!-- end row -->




                    </div> <!-- end container-fluid -->

                </div> <!-- end content -->


            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

@endsection
