@extends('layouts.regapp')
@section('title')
 Jobs in South Africa  | Openjobs360
@endsection
@section('content')

<!-- Our Candidate List -->
    <section class="our-faq bgc-fa mt50">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xl-3 dn-smd">
                     <div class="faq_search_widget mb30">
                        <h4 class="fz20 mb15">Search for Jobs </h4>
                         <form method="GET" action="{{route('search')}}" class="form-inline">
                         {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <input type="text" name="query" class="form-control" placeholder="Find Your Job" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary"  type="submit" id="button-addon2"><span class="flaticon-search"></span></button>
                            </div>
                        </div>
                    </form>
                    </div>



                    <div class="cl_latest_activity mb30">
                        <h4 class="fz20 mb15">Find Jobs By Area</h4>
                    @foreach ($areas as $country)

                     @foreach ($country->children as $state)
                        <h5><a href="{{ route('listings.inarea', $state) }}">{{ $state->name }}</a></h5>
                     @endforeach
                    @endforeach
                    </div>





                </div>
                <div class="col-lg-9 col-xl-9">
                    <div class="row">
                        <div class="col-lg-12 mb30">
                            <h4 class="fz20 mb15">Browse Jobs By Area</h4>
                            <div class="tags-bar">
                                 <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a>
                                 <a href="#"><i class="fa fa-arrow-right"></i> Jobs By Area


                                  </a>


                                <div class="action-tags">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12 col-lg-6">

                            <div class="content_details">
                                <div class="details">

                                    <div class="faq_search_widget mb30">
                                        <h4 class="fz20 mb15">Search Keywords</h4>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Find Your Question" aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon4"><span class="flaticon-search"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ($listings->count())
                  @foreach ($listings as $listing)
            @include ('listings.partials.base', compact('listing'))
                      @endforeach


                   {{ $listings->links() }}
              @else
                 <p>No Jobs listed in here at the moment.</p>
                     @endif


                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
