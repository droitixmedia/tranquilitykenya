
<div class="col-md-4">
                                            <div class="card-pricing text-center card pb-4 mt-4">
                                                 @if (!$comment->approvals->count())
                                                <div class="pricing-header bg-secondary rounded-top">
                                                    
                                                    <h6 class="card-price text-white pt-4"> ZMK{{$comment->split}}</h6>

                                                </div>
                                                @else

                                                  <div class="pricing-header bg-success rounded-top">
                                                    <h1 class="card-price text-white pt-4">Payement verified</h1>
                                                    <h6 class="card-price text-white pt-4"> K{{$comment->split}}</h6>

                                                </div>

                                                @endif

                                                 @if (!$comment->approvals->count())
                                                <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                    <li>YOUR DEPOSIT HAS BEEN MATCHED PAY PERSON BELOW</li>
                                                    <li>NAMES: {{$comment->listing->user->name}} {{$comment->listing->user->surname}}</li>
                                                    <li>CONTACT NUMBER: {{$comment->listing->user->phone}}</li>
                                                    <li>BANK OR MOBILE MONEY: {{$comment->listing->user->bank}}</li>
                                                    <li>ACCOUNT NUMBER: {{$comment->listing->user->account}}</li>
                                                    <hr>
                                                     


                                                </ul>
                                                  @else
                                                  <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                   <li><h5>{{$comment->listing->user->name}} Approved You</h5></li>
                                               </ul>

                     <form action="{{ route('listings.store', [$area]) }}" method="post">

                                           <input type="hidden" class="form-control" name="amount" id="amount" value="{{($comment->category->parent->percent*$comment->split)}}">
                                           <input type="hidden" class="form-control" name="maturityamount" id="maturityamount" value="{{($comment->category->parent->percent*$comment->split)}}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="type" id="type" value="1">
                                         <input type="hidden" class="form-control" name="category_id" id="category" value="2">
                                         <input type="hidden" class="form-control" name="secret" id="category" value="{{$comment->id}}">
                                          <input type="hidden" class="form-control" name="updated_at" id="created" value="{{$comment->created_at}}">

                                             <button type="submit" class="btn btn-outline-success">Check Progress</button>

                                         {{ csrf_field() }}
                                    </form>


                    @endif
                            @if (!$comment->approvals->count())
                    <a href="#" class="get-started-btn"></a>
                      @else
                     <a href="#" class="get-started-btn"></a>
                        @endif
                                            </div> <!-- end Pricing_card -->
                                        </div> <!-- end col -->

