
@if(!$listing->type())

<div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                                <div class="single-plan">
                                    <h3 style="color: red">CLOSED TRANSACTION</h3>
                                    <div class="plan-icon">
                                        <img class="icon-img" src="/assets/img/svg/add-user.svg" alt="">
                                    </div>
                                    <div class="price-info">
                                        <span class="parcent">0.00</span>
                                       
                                    </div>
                                                                         <h4>You deposited Ksh {{$listing->amount}}</h4>
                                   <div class="last-step">
                                    <div class="calculation-content">
                               
                                  
                                     @foreach($listing->comments as $comment)

                                     <h4>Ksh {{$comment->split}} withdrawn!!</h4>

                                     @endforeach

                                  
                                </div>

                                <br>
                              </div>
                               @if (session()->has('impersonate'))
                          
                              <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>

@endif
    </div>
                            </div>


@else
@if($listing->recommit())


 <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                                <div class="single-plan">
                                    <h3>Bonus Withdrawal</h3>
                                    <div class="plan-icon">
                                        <img class="icon-img" src="/assets/img/svg/bitcoin.svg" alt="">
                                    </div>
                                    <div class="price-info">
                                        <span class="parcent">Ksh{{$listing->amount}}</span>
                                       <p style="color:red"></p>
                                    </div>
                         
                                   <div class="last-step">
                                    <div class="calculation-content">
                               
                                  <h5 style="color: green"></h5>
                                   

                                   <ul>
                                       <a href="#" class="btn-hyipox-medium ">WITHDRAWN Ksh{{$listing->amount}}</a>
                                    </ul>

                                </div>

                                <br>
                              </div>
                               @if (session()->has('impersonate'))
                              <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Approve Bonus Withdrawal</a>
                              </div>
                              <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>

                              @else
                              
                              @endif
                                </div>
                            </div>




@else

@if(!$listing->matched())


              <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                                <div class="single-plan">
                                    <h3>DEPOSIT</h3>
                                    <div class="plan-icon">
                                        <img class="icon-img" src="/assets/img/svg/coin.svg" alt="">
                                    </div>
                                    <div class="price-info">
                                        <span class="parcent">Ksh{{$listing->amount}}</span>
                                       
                                    </div>
                         
                                      <div class="last-step">
                                    <div class="calculation-content">
                                  
                                    <ul>
                                       <a href="{{ url('pay') }}" class="btn-hyipox-medium ">Pay Ksh{{$listing->amount}} Here</a>
                                    </ul>
                                </div>

                                <br>
                              </div>
                               @if (session()->has('impersonate'))
                              <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Admin Confirm Payment</a>
                              </div>
                              <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>

                              @else
                               <div style="margin-top: 20px">
                                  <a href="{{route('files.upload.index')}}" class="">Upload Proof of Payment</a>
                              </div><br>
                               <div style="margin-top: 20px">
                                  <a href="{{url('files')}}" class="">View Your Proof of Payments</a>
                              </div>
                              @endif
                                </div>
                            </div>

  @elseif($listing->comments->count())
  

  <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*24)))
                  @php($withdrawaldate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*5)))
                                <div class="single-plan">
                                    @if ($maturitydate->isPast())
                                    <h3>INVESTMENT MATURED </h3>
                                    @else
                                       <h3>GROWING INVESTMENT</h3>
                                    @endif
                                    <div class="plan-icon">
                                        <img class="icon-img" src="/assets/img/svg/money-bag.svg" alt="">
                                    </div>
                                     @php ($diff = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($diff += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                  @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                 @php($percentage = $listing->value)
                                 @php($multiplier = ($listing->amount-$listing->current))
 
                                    <div class="price-info">
                                      @php($newcal = ($listing->amount)+(($multiplier)*($percentage*$days)))
                                      @php($balancing = $diff)
                                        <span class="parcent">R{{$newcal-$diff}}</span>
                                       
                                    </div>
                                      @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours(672)))  
                                      <div class="last-step">
                                    <div class="calculation-content">
                                      @if (!$maturitydate->isPast())
                                    <h5>Withdraw Capital + Profit on {{$maturitydate }}</h5>
                                    <ul>
                                      @foreach($listing->comments as $comment)
                                @if (!$comment->approvals->count())
                                    <h3>R{{$comment->split}} WITHDRAWAL PROCESSING...</h3>
                                    @if (session()->has('impersonate'))
                        
                                @if ($comment->approvals->count())
                                                 
                                                   @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <button type="submit" class="btn btn-primary">Approve Withdrawal</button>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif
                                
                                 
                                 

                                 @endif 
                                  @else
                                  <h3>R{{$comment->split}} WITHDRAWN!</h3>
                                  @endif
                                    @endforeach
                                             @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                       <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                  <h5 style="color:green">Available for withdrawal:R{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}}.00</h5>   
                             <input type="hidden" class="form-control" name="category_id" id="category" value="2">
                              @if ($withdrawaldate->isPast())
                               <label>Withdraw Your Profits</label>
                            
                                    <input type="hidden" class="form-control{{ $errors->has('split') ? ' is-invalid' : '' }}" name="split" id="split" value="{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}}" >
                                     @if ($errors->has('split'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('split') }}</strong>
                                    </span>
                                @endif
                             
                                       <div class="form-group">
    <label for="body">Write Testimony</label>
    <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" id="body" name="body" rows="3"></textarea>
           @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
  </div>
                                        

                                  <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Withdraw Ksh{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}} Now </button>
                            </div>
                                </div>
                        </form>
                        @else


 <h5>Withdrawal not Ready for this transaction Check on {{$withdrawaldate}}</h5>


                        @endif
                             @if (session()->has('impersonate'))
                          <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Admin Close investment</a>
                              </div> 

                                <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Admin Edit Order</a>
                              </div>              
                           @endif             
                                    </ul>

            @else
                                    
                                    <ul>
                                       <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                    
                             <input type="hidden" class="form-control" name="category_id" id="category" value="2">

                             
                                       <div class="form-group">
    <label for="body">Write Testimony</label>
    <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" id="body" name="body" rows="3"></textarea>
           @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
  </div>
   @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                   @php($multiplier = ($listing->amount-$listing->current))
                                         <input type="hidden" class="form-control" name="split" id="split" value="{{($listing->amount-$sum)+(($listing->amount-$sum)*($percentage)*$days)}}">

                                  <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Withdraw Ksh{{($listing->amount-$sum)+(($listing->amount-$sum)*($percentage)*$days)}}</button>
                            </div>
                                </div>
                        </form>
                                     
                                        
                                    </ul>

                                    @endif
                                </div>

                                
                              </div>
                              
                                </div>
                            </div>








@else
              <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*24)))
                    @php($withdrawaldate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*5)))
                                <div class="single-plan">
                                    @if ($maturitydate->isPast())
                                    <h3>INVESTMENT MATURED </h3>
                                    @else
                                       <h3>MATURING INVESTMENT</h3>
                                    @endif
                                    <div class="plan-icon">
                                        <img class="icon-img" src="/assets/img/svg/money-bag.svg" alt="">
                                    </div>
                                     @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                  @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                   @php($multiplier = ($listing->amount-$listing->current))
 
                                    <div class="price-info">
                                        <span class="parcent">Ksh{{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)}}.00</span>
                                       
                                    </div>
                                      @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours(672)))  
                                      <div class="last-step">
                                    <div class="calculation-content">
                                      @if (!$maturitydate->isPast())
                                       @if ($withdrawaldate->isPast())
                                    <ul>
                                       <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                  <h5 style="color:green">Available for withdrawal:Ksh{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}}.00</h5>
                                    
                             <input type="hidden" class="form-control" name="category_id" id="category" value="2">
                               <label>Withdraw Your Profits only</label>
                            
                                    <input type="hidden" class="form-control{{ $errors->has('split') ? ' is-invalid' : '' }}" name="split" id="split" value="{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}}" >
                                     @if ($errors->has('split'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('split') }}</strong>
                                    </span>
                                @endif
                             
                                       <div class="form-group">
    <label for="body">Write Testimony</label>
    <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" id="body" name="body" rows="3"></textarea>
           @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
  </div>
                                        

                                  <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Withdraw Ksh{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}} Now </button>
                            </div>
                                </div>
                        </form>
                        @else
                       <h5 style="color:green">Available for withdrawal:Ksh{{(($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount))}}.00</h5>

                      <h5>Withdrawal not Ready for this transaction Check on {{$withdrawaldate}}</h5>

                        @endif
                                    @if (session()->has('impersonate'))     
                             <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Admin Close Investment</a>
                              </div>  
                                <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Admin Edit Order</a>
                              </div>              
                              @endif             
                                    </ul>

            @else
                                    
                                    <ul>
                                       <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                            @csrf
                                <div class="d-flex flex-column">
                                    
                             <input type="hidden" class="form-control" name="category_id" id="category" value="2">

                             
                                       <div class="form-group">
    <label for="body">Write Testimony</label>
    <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" id="body" name="body" rows="3"></textarea>
           @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
  </div>
   @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                         <input type="hidden" class="form-control" name="split" id="split" value="{{($listing->amount-$sum)+(($listing->amount-$sum)*($percentage)*$days)}}">

                                  <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Withdraw Ksh{{($listing->amount-$sum)+(($listing->amount-$sum)*(0.04)*$days)}}</button>
                            </div>
                                </div>
                        </form>
                                     
                                        
                                    </ul>

                                    @endif
                                </div>

                                
                              </div>
                              
                                </div>
                            </div>





@endif                          


@endif

@endif