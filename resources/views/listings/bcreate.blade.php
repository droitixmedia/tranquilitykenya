@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Bonus Withdrawal</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Bonus</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="col-xl-7 col-lg-7 col-md-7">
                           
                            <div class="last-step">
                                <div>@php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                                    <h5>Your Bonus Balance is Ksh{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-$withdrawn}}.00</h5>
                                                                   <form action="{{ route('listings.store', [$area]) }}" method="post">
                                   
                                    <input type="hidden" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-$withdrawn}}">
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                        
                                          <input type="hidden" class="form-control" name="value" id="value" value="1">
                                           <input type="hidden" class="form-control" name="current" id="value" value="1">
                                         <input type="hidden" class="form-control" name="recommit" id="period" value="1">
                                         <input type="hidden" class="form-control" name="period" id="period" value="1">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                 <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Withdraw Bonus</button>
                            </div>
                             {{ csrf_field() }}
                                </form>
                                </div>
                                <div class="calculation-content">
                                    <h4 class="title">Bonus Rules</h4>
                                    <ul>
                                        <li>
                                            <i class="far fa-check-circle"></i>
                                            <span class="list-title">
                                                Minimum Bonus Withdrawal
                                            </span>
                                            <span class="list-descr">Ksh500</span>
                                        </li>
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                           
                        </div>
                            
                        </div>
                    </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
