<div class="col-sm-12 col-lg-9">
    <a href="{{ route('listings.show', [$area, $listing]) }}">
                    <div class="fj_post">
                        <div class="details">
                            <h5 class="job_chedule text-thm mt0">{{$listing->position}}</h5>
                            <div class="thumb fn-smd">
                                <img class="img-fluid" src="images/partners/1.jpg" alt="1.jpg">
                            </div>
                            <h4>{{$listing->jobtitle}}</h4>
                            <p>Posted {{$listing->created_at->diffForHumans()}} by <a class="text-thm" href="#">{{$listing->companyname}}</a></p>
                            <ul class="featurej_post">
                                <li class="list-inline-item"><span class="flaticon-location-pin"></span> <a href="{{ route('listings.show', [$area, $listing]) }}">{{ $listing->area->parent->name }}, {{ $listing->area->name }}</a></li>
                                <li class="list-inline-item"><span class="flaticon-price pl20"></span> <a href="{{ route('listings.show', [$area, $listing]) }}">{{$listing->salary}}</a></li>
                            </ul>
                        </div>
                        <a class="btn btn-md btn-transparent float-right fn-smd" href="{{ route('listings.show', [$area, $listing]) }}">View and Apply</a>
                    </div>
                    </a>
                </div>
