@extends('layouts.userapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
               
            </div>
            <!-- breadcrumb end -->

  <!-- faq begin -->
            <div class="faq">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-xl-3 col-lg-3 col-md-4">
                            <div class="faq-sidebar">
                               
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active" id="v-pills-fnb-tab" data-toggle="pill" href="#v-pills-fnb" role="tab" aria-controls="v-pills-fnb" aria-selected="true">MPESA</a>
                                    
                                  
                                    <a class="nav-link" id="v-pills-btc-tab" data-toggle="pill" href="#v-pills-btc" role="tab" aria-controls="v-pills-btc" aria-selected="false">BITCOIN</a>
                                    <a class="nav-link" id="v-pills-skrill-tab" data-toggle="pill" href="#v-pills-skrill" role="tab" aria-controls="v-pills-skrill" aria-selected="false">SKRILL</a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <div class="faq-content">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-fnb" role="tabpanel" aria-labelledby="v-pills-fnb-tab">
                                        <div class="single-faq">
                                            <h4>Use our MPESA  details below to make your deposit.</h4>
                                            <h5>MPESA</h5>

                                           <h5 style="color: green">MOBILE MONEY NUMBER: 0795661283</h5>
                                            <h5 style="color: green">ACCOUNT NAME: DERRICK BOSIRE </h5>
                                            

                                            <h5 style="color: red">SEND YOUR PROOF OF PAYMENT TO THIS WHATSAPP! +27642971130</h5>
                                             <a href="{{route('files.upload.index')}}" class="btn-hyipox-2">Upload Proof of Payement</a>
                                        </div>
    
                                       
                                    </div>
    
                                    <div class="tab-pane fade" id="v-pills-capitec" role="tabpanel" aria-labelledby="v-pills-capitec-tab">
                                        <div class="single-faq">
                                            <h4>Use our CAPITEC Bank  details below to make your deposit.</h4>
                                            <h5>CAPITEC</h5>

                                           <h5 style="color: green">ACCOUNT NUMBER:  1821357475</h5>
                                            <h5 style="color: green">ACCOUNT TYPE:  CURRENT ACCOUNT</h5>
                                             <h5 style="color: green"> </h5>

                                            <h5 style="color: red">SEND YOUR PROOF OF PAYMENT TO THIS WHATSAPP! 0642971130</h5>
                                             <a href="{{route('files.upload.index')}}" class="btn-hyipox-2">Upload Proof of Payement</a>
                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-standard" role="tabpanel" aria-labelledby="v-pills-standard-tab">
                                        <div class="single-faq">
                                            <h4>Use our STANDARD Bank  details below to make your deposit.</h4>
                                            <h5>STANDARD BANK</h5>

                                           <h5 style="color: green">ACCOUNT NUMBER: 10139444082</h5>
                                            <h5 style="color: green">ACCOUNT TYPE:  CURRENT ACCOUNT</h5>
                                             

                                            <h5 style="color: red">SEND YOUR PROOF OF PAYMENT TO THIS WHATSAPP! 0642971130</h5>
                                             <a href="{{route('files.upload.index')}}" class="btn-hyipox-2">Upload Proof of Payement</a>
                                        </div>
                                       
                                    </div>
                                    
                                   
                                     <div class="tab-pane fade" id="v-pills-btc" role="tabpanel" aria-labelledby="v-pills-btc-tab">
                                        <div class="single-faq">
                                            <h4>Use our BITCOIN  ADDRESS BELOW.</h4>
                                            <h5>BITCOIN</h5>

                                           <h5 style="color: green">Wallet Address: bc1qrcp05ptkkn7mkzkw66775gahr7xxe26ccf464c</h5>
                                           

                                            <h5 style="color: red">SEND YOUR PROOF OF PAYMENT TO THIS WHATSAPP! 0642971130</h5>
                                             <a href="{{route('files.upload.index')}}" class="btn-hyipox-2">Upload Proof of Payement</a>
                                        </div>
    
                                        
                                    </div>
                                          <div class="tab-pane fade" id="v-pills-skrill" role="tabpanel" aria-labelledby="v-pills-skrill-tab">
                                        <div class="single-faq">
                                            <h4>Use our SKRILL  ADDRESS BELOW.</h4>
                                            <h5>SKRILL</h5>

                                           <h5 style="color: green">Email:  payment@tranquilityforex.org</h5>
                                          

                                            <h5 style="color: red">SEND YOUR PROOF OF PAYMENT TO THIS WHATSAPP! 0642971130</h5>
                                             <a href="{{route('files.upload.index')}}" class="btn-hyipox-2">Upload Proof of Payement</a>
                                        </div>
    
                                        
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <div class="single-faq">
                                            <h4>Do I need to register to play?</h4>
                                            <p>It is a long established fact that a reader will be distracted by the readable content of a page whene at
                                                layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters too
                                                using eader will be distracted by the readable conten.</p>
                                        </div>
    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- faq end -->

           


@endsection
