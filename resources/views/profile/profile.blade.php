@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Profile Information</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Profile</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                                <div class="part-data">
                                    <span class="name">{{ Auth::user()->name}} {{ Auth::user()->surname}}</span>
                                   
                                </div>
                                <div class="part-img">
                                    <img src="/uploads/avatars/{{ $user->avatar }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

          <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    
                

                    <!-- profile begin -->
                    <div class="profile">
                            <div class="row justify-content-center">
                                <div class="col-xl-3 col-lg-3">
                                    <div class="accont-tab">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" id="personal-info-tab" data-toggle="pill" href="#personal-info" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="far fa-user"></i> Personal Info</a>
                                            <a class="nav-link" id="payment-info-tab" data-toggle="pill" href="#payment-info" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fas fa-credit-card"></i> Edit Profile</a>
                                            
                                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-cog"></i>Change Password</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-9">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        <div class="tab-pane fade show active" id="personal-info" role="tabpanel" aria-labelledby="personal-info-tab">
                                            <h3 class="title">Edit Your Profile</h3>
                                            <div class="player-profile">
                                                <div class="row no-gutters">
                                                    <div class="col-xl-4 col-lg-4 col-md-4">
                                                        <div class="player-card">  
                                                            <div class="part-pic">
                                                                <img src="/uploads/avatars/{{ $user->avatar }}" alt="">
                                                            </div>
                                                            <div class="social-link">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-8 col-lg-8 col-md-8">

                                                        <div class="part-info">
                                                            <span class="player-name">
                                                                {{ Auth::user()->name}} {{ Auth::user()->surname}}
                                                               
                                                            </span>

                         
                         
                                                            <ul>

                                                                  <div class="col-lg-12">
                                <h4 class="fz20 mb20">Change Your Profile Picture</h4>
                            </div>
                        <div class="my_profile_input form-group">
                     <form enctype="multipart/form-data" action="{{route('profile.update.avatar')}}" method="POST">
                                         <input  type="file" style="margin-left: 20px;" name="avatar">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
                  <div class="edit-profile text-center">
                                                <button type="submit" class="btn-hyipox-2">Update</button>
                                            </div>
               </form>
           
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div class="tab-pane fade" id="payment-info" role="tabpanel" aria-labelledby="payment-info-tab">
                                            <h3 class="title">Edit Profile Info</h3>
                                    <div class="responsive">
                                                    <table class="table table-bordered">
                                                     <form method="POST" action="{{route('profile.update')}}">
                        @csrf

                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">First Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" value="{{$user->name}}" class="form-control" name="name" autocomplete="">
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" value="{{$user->surname}}" class="form-control" name="surname" autocomplete="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" value="{{$user->email}}" class="form-control" name="email" autocomplete="">
                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" value="{{$user->phone_number}}" class="form-control" name="phone_number" autocomplete="">
                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="bank" class="col-md-4 col-form-label text-md-right">Your Bank/Mobile Money</label>

                            <div class="col-md-6">
                                <input id="bank" type="text" value="{{$user->bank}}" class="form-control" name="bank" autocomplete="">
                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="account" class="col-md-4 col-form-label text-md-right">Account Number</label>

                            <div class="col-md-6">
                                <input id="account" type="text" value="{{$user->account}}" class="form-control" name="account" autocomplete="">
                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="accounttype" class="col-md-4 col-form-label text-md-right">Skrill Email</label>

                            <div class="col-md-6">
                                <input id="accounttype" type="text" value="{{$user->accounttype}}" class="form-control" name="accounttype" autocomplete="">
                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="btcaddress" class="col-md-4 col-form-label text-md-right">Bitcoin Address</label>

                            <div class="col-md-6">
                                <input id="btcaddress" type="text" value="{{$user->btcaddress}}" class="form-control" name="btcaddress" autocomplete="">
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="phone" value="phone">




                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Profile
                                </button>
                            </div>
                        </div>
                    </form>
                                                    </table>
                                                </div>
</div>
                                       
                                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                            <div class="account-settings">
                                                <h3 class="title">Change Password</h3>
                                                <div class="responsive">
                                                    <table class="table table-bordered">
                                                     <form method="POST" action="{{ route('change.password') }}">
                        @csrf

                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" >
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- profile end -->
                    
                </div>
            </div>
            <!-- account end -->

@endsection
