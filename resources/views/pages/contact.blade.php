@extends('layouts.userapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-9 col-lg-7 col-8">
                            <div class="part-txt">
                                <h1>How to Contact Us</h1>
                                <ul>
                                    <li>home</li>
                                    <li>contact</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-4 d-flex align-items-center">
                            <div class="part-img">
                                <img src="/assets/img/breadcrumb-img.png" alt="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- contact begin -->
            <div class="contact contact-page" id="contact">
                <div class="container container-contact">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title">
                                <span class="sub-title">
                                    Contact Us
                                </span>
                                <h2>
                                    Get in touch<span class="special"> with us</span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="bg-tamim">
                        <div class="row justify-content-around">
                            <div class="col-xl-6 col-lg-6 col-sm-10 col-md-6">
                                <div class="part-form">
                                    <form>
                                        <input type="text" placeholder="Your Name">
                                        <input type="text" placeholder="Your Email">
                                        <textarea placeholder="Write to Us..."></textarea>
                                        <button class="submit-btn def-btn" type="submit">Send</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6">
                                <div class="part-address">
                                    <div class="addressing">
                                        
                                        <div class="single-address">
                                            <h4>Emails</h4>
                                            <p>
                                               support@tranquilityforex.org</p>
                                        </div>
                                        <div class="single-address">
                                            <h4>Phone</h4>
                                            <p>+27818688017<br/>
                                                +27815121789</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- contact end -->


           


@endsection
