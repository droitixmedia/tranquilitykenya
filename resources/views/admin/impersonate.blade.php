@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Admin Impersonate</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Impersonate</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="col-xl-7 col-lg-7 col-md-7">
                           
                            <div class="last-step">
                              <form action="{{ route('admin.impersonate') }}" method="POST">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>

                            <button type="submit" class="btn btn-thm">Impersonate</button>
                            {{ csrf_field() }}
                        </form>
                            </div>
                           
                        </div>
                            
                        </div>
                    </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
