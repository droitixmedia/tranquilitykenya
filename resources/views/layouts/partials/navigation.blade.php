
            <!-- header begin -->
            <div class="header header-style-4" id="header">
                <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
 
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "proName": "FOREXCOM:SPXUSD",
      "title": "S&P 500"
    },
    {
      "proName": "FOREXCOM:NSXUSD",
      "title": "US 100"
    },
    {
      "proName": "FX_IDC:EURUSD",
      "title": "EUR/USD"
    },
    {
      "proName": "BITSTAMP:BTCUSD",
      "title": "Bitcoin"
    },
    {
      "proName": "BITSTAMP:ETHUSD",
      "title": "Ethereum"
    }
  ],
  "showSymbolLogo": true,
  "colorTheme": "dark",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "en"
}
  </script>
</div>
                <div class="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-5 col-lg-3">
                                <div class="welcome-text">
                                    <p>Welcome to Tranquility</p>
                                </div>
                            </div>

                            <div class="col-xl-7 col-lg-9 d-xl-flex d-lg-flex d-block align-items-center">
                                <div class="part-right">
                                    <ul>
                                        <li>
                                            <span class="simple-text">Current Time : </span>
                                            <div class="server-time">
                                                <div class="single-time clock-time">
                                                    <span class="icon">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                    <span class="text">
                                                        <span id="hours"></span>
                                                        <span id="minutes"></span>
                                                        <span id="seconds"></span>
                                                    </span>
                                                </div>
                                                <div class="single-time">
                                                    <span class="icon">
                                                        <i class="fas fa-calendar-alt"></i>
                                                    </span>
                                                    <span class="text">
                                                        <span id="date"></span>
                                                        <span id="month"></span>
                                                        <span id="year"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            @guest
                                            <div class="user-panel">
                                                <span>
                                                    <a href="{{url('login')}}" class="login-btn">Login</a>or</span><a href="{{url('register')}}" class="register-btn">Register</a>
                                            </div>
                                            @else

                                              <div class="user-panel">
                                                <span>
                                                   <a href="{{ route('listings.create', [$area]) }}" class="register-btn">Invest Now</a>
                                            </div>

                                            @endguest
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bottom">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-3 col-lg-2 d-xl-flex d-lg-flex d-block align-items-center">
                                <div class="row">
                                    <div class="col-4 d-xl-none d-lg-none d-block">
                                        <button class="navbar-toggler" type="button">
                                            <span class="dag"></span>
                                            <span class="dag2"></span>
                                            <span class="dag3"></span>
                                        </button>    
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-8 d-xl-block d-lg-block d-flex align-items-center justify-content-end">
                                        <div class="logo">
                                            <a href="{{url('/dashboard')}}">
                                                <img src="/assets/img/logo2.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="mainmenu">
                                    <div class="d-xl-none d-lg-none d-block">
                                        <div class="user-profile">
                                            @guest
                                              <div class="part-img">

                                                <img src="/assets/img/user.png" alt="">
                                            </div>
                                            <div class="log-out-area">
                                                <a href="{{url('login')}}">Login</a>
                                            </div>
                                            @else
                                            <div class="part-img">

                                                <img src="/uploads/avatars/{{Auth::user()->avatar}}" alt="">
                                            </div>
                                            <div class="user-info">
                                                <span class="user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
                                                               
                         @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                            @php ($sum += ($listing->amount)+(($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                                                <span class="user-balance">Bal : Ksh {{$sum-$diff}}</span>
                                            </div>
                                            <div class="log-out-area">
                                                <a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">Log out</a>
                                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                            </div>
                                            @endguest
                                        </div>
                                    </div>
                                    <nav class="navbar navbar-expand-lg">              

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="navbar-nav ml-auto">
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('/')}}"><i class="fas fa-h-square"></i>Home</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{url('dashboard')}}"><i class="fas fa-barcode"></i>Dashboard<span class="sr-only">(current)</span></a>
                                                </li>
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('listings.create', [$area]) }}"><i class="fas fa-file-invoice-dollar"></i>Invest<span class="sr-only">(current)</span></a>
                                                </li>
                                                @role('admin')
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/impersonate')}}"><i class="fas fa-lock"></i>Impersonate<span class="sr-only">(current)</span></a>
                                                </li>
                                                  <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/users')}}"><i class="fas fa-lock"></i>Users<span class="sr-only">(current)</span></a>
                                                     <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/withdrawals')}}"><i class="fas fa-lock"></i>Withdrawals</a>
                                                </li>

                                                </li>
                                                   <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/investments')}}"><i class="fas fa-lock"></i>Invested</a>
                                                </li>
                                     
                                               <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/listings')}}"><i class="fas fa-lock"></i>Deposits</a>
                                                </li>
                                                @endrole
                                                 @if (session()->has('impersonate'))
                                                      <li class="nav-item">
                                <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Stop Impersonating </span>
                                </a>
                                <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                           </li>
                                                 @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{url('profile')}}"><i class="fas fa-user"></i> Profile<span class="sr-only">(current)</span></a>
                                                </li>

                                               
                                                @guest
                                                <li class="nav-item join-now-btn">
                                                    <a class="nav-link" href="{{url('login')}}">Login</a>
                                                </li>
                                                @else

                                                <li class="nav-item notification">
                                                    <a class="nav-link" href="{{route('notifications')}}">
                                                        <i class="fa fa-bell">Notifications</i>
                                                        <sub style="color:red">{{request()->user()->unreadNotifications()->count()}}</sub> 
                                                    </a>
                                                </li>

                                                @endguest
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header end -->
