      <!-- header begin -->
            <div class="header" id="header">
                <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "proName": "FOREXCOM:SPXUSD",
      "title": "S&P 500"
    },
    {
      "proName": "FOREXCOM:NSXUSD",
      "title": "US 100"
    },
    {
      "proName": "FX_IDC:EURUSD",
      "title": "EUR/USD"
    },
    {
      "proName": "BITSTAMP:BTCUSD",
      "title": "Bitcoin"
    },
    {
      "proName": "BITSTAMP:ETHUSD",
      "title": "Ethereum"
    }
  ],
  "showSymbolLogo": true,
  "colorTheme": "dark",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "en"
}
  </script>
</div>
                <div class="bottom">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-xl-3 col-lg-2 d-xl-flex d-lg-flex d-block align-items-center">
                                <div class="row">
                                    <div class="col-6 d-xl-none d-lg-none d-block">
                                        <button class="navbar-toggler" type="button">
                                            <span class="dag"></span>
                                            <span class="dag2"></span>
                                            <span class="dag3"></span>
                                        </button>    
                                    </div>
                                    <div>
                                        <div class="logo">
                                            <a href="{{url('/')}}">
                                                <img src="assets/img/logo.png"  alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="mainmenu">
                                    <div class="d-xl-none d-lg-none d-block">
                                        <div class="user-profile">
                                            @guest
                                              <div class="part-img">

                                                <img src="assets/img/user.png" alt="">
                                            </div>
                                            <div class="log-out-area">
                                                <a href="{{url('login')}}">Login</a>
                                            </div>
                                            @else
                                            <div class="part-img">

                                                <img src="/uploads/avatars/{{Auth::user()->avatar}}" alt="">
                                            </div>
                                            <div class="user-info">
                                                <span class="user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
                           @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                            @php ($sum += ($listing->amount)+(($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                                                <span class="user-balance">Bal : Ksh{{$sum-$diff}}</span>
                                            </div>
                                            <div class="log-out-area">
                                                <a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">Log out</a>
                                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                            </div>
                                            @endguest
                                        </div>
                                    </div>
                                    <nav class="navbar navbar-expand-lg">              

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="navbar-nav ml-auto">
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('/')}}"><i class="fas fa-h-square"></i>Home</a>
                                                </li>
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('/fx')}}"><i class="fas fa-h-square"></i>Fx Market</a>
                                                </li>
                                             
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{url('about')}}"><i class="fas fa-globe-africa"></i>About<span class="sr-only">(current)</span></a>
                                                </li>
                                                  @guest
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('listings.create', [$area]) }}"><i class="fas fa-briefcase"></i>Invest Now<span class="sr-only">(current)</span></a>
                                                </li>
                                                @else
                                                 <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('dashboard') }}"><i class="fas fa-barcode"></i>Dashboard<span class="sr-only">(current)</span></a>
                                                </li>

                                                 @endguest
                                     
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{url('contact')}}"><i class="fas fa-comment-alt"></i>Contact Us</a>
                                                </li>
                                                @guest
                                                <li class="nav-item join-now-btn">
                                                    <a class="nav-link" href="{{url('login')}}">Login</a>
                                                </li>
                                                @else

                                                 <li class="nav-item join-now-btn">
                                                    <a class="nav-link" href="{{ route('listings.create', [$area]) }}">Invest Now</a>
                                                </li>
                                                @endguest
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header end -->