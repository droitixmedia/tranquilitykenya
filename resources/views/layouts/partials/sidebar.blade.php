
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">
                               <a href="{{url('/dashboard')}}">
                            <li > <h4 style="color:#f4faff;"><i class="mdi mdi-speedometer"></i> Office Menu<h4></li>
                               <li></a>
                                <a href="{{url('/dashboard')}}">
                                    
                                    <span> ACTIONS </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/change-password')}}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Change Password </span>
                                </a>
                            </li>
                             <li>
                                <a href="{{url('profile')}}">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Change Wallet </span>
                                </a>
                            </li>
                                 <li>
                                <a href="{{url('profile')}}">
                                    <i class="mdi mdi-phone"></i>
                                    <span> Change Phone </span>
                                </a>
                            </li>
                              <li>
                                <a href="{{ route('listings.create', [$area]) }}">
                                    <i class="mdi mdi-hand-pointing-up"></i>
                                    <span> Deposit </span>
                                </a>
                            </li>
                               <li>
                                <a href="{{ route('listings.published.index', [$area]) }}">
                                    <i class="mdi mdi-hand-pointing-down"></i>
                                    <span> Withdraw </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('listings.published.index', [$area]) }}">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Get Bonus </span>
                                </a>
                            </li>
                          <li>
                              <li>
                                <a href="{{url('/')}}">
                                    <i class="mdi mdi-message"></i>
                                    <span> Support </span>
                                </a>
                            </li>
                               <li>
                                <a href="{{url('profile')}}">
                                    <i class="mdi mdi-account-outline"></i>
                                    <span> Profile </span>
                                </a>
                            </li>
                              <li>
                                <a href="{{ route('comments.published.index') }}">
                                    <i class="mdi mdi-folder"></i>
                                    <span> Inbox </span>
                                </a>
                            </li>
                          <li>

                             @role('admin')

                            <li>
                                <a href="{{ route('listings.bcreate', [$area]) }}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Create Bid </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('admin/impersonate')}}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Impersonate </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('admin/users')}}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Users </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('admin/listings')}}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Listings</span>
                                </a>
                            </li>


                             @endrole
                         @if (session()->has('impersonate'))
                         <li>
                                <a href="{{ route('listings.bcreate', [$area]) }}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Create Bid </span>
                                </a>
                            </li>
                          <li>
                             <li>
                                <a href="{{ url('bidding') }}">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Admin Match </span>
                                </a>
                            </li>
                          <li>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Stop Impersonating </span>
                                </a>
                                <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                           </li>


                         @endif
                      

                              <li>
                                <a  href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                             <i class="mdi mdi-logout"></i><span class="menu-title">{{ __('Logout') }}</span>
                                        </a>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                            </li>


                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!--
