<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.head')

</head>
  <body>
   
        <div class="notification-alert">
            <div class="notice-list">
                
            </div>
        </div>

  <div id="app">
        <!-- preloader end -->
        
        <div class="mobile-navbar-wrapper">

   @include('layouts.partials.offlinenav')




       @yield('content')
</div> <!-- mobile navbar wrapper end -->

        <div class="d-xl-none d-lg-none d-block">
            <div class="mobile-navigation-bar">
             <ul>
                    <li>
                        <a href="{{url('profile')}}">
                            <img src="/assets/img/svg/profile.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('listings.create', [$area]) }}">
                            <img src="/assets/img/svg/money-transfering.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{{url('dashboard')}}">
                            <img src="/assets/img/svg/investor.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#header">
                            <img src="/assets/img/svg/arrow.svg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="d-xl-block d-lg-block d-none">
            <div class="back-to-top-btn">
                <a href="#">
                    <img src="/assets/img/svg/arrow.svg" alt="">
                </a>
            </div>
        </div>

@include('layouts.partials.footer')
 <!-- jquery -->
        <script src="/assets/js/jquery.js"></script>
        <!-- popper js -->
        <script src="../../../../cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <!-- bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <!-- modal video js -->
        <script src="/assets/js/jquery-modal-video.min.js"></script>
        <!-- slick js -->
        <script src="/assets/js/slick.min.js"></script>
        <!-- toastr js -->
        <script src="/assets/js/toastr.min.js"></script>
        <!-- clock js -->
        <script src="/assets/js/clock.min.js"></script>
        <!-- main -->
        <script src="/assets/js/main.js"></script>

         <!-- jquery -->
        <script src="/assets/js/jquery.js"></script>
        <!-- popper js -->
        <script src="/assets/js/popper.min.js"></script>
        <!-- bootstrap -->

        <script src="/assets/js/bootstrap.min.js"></script>
        <!-- modal video js -->
        <script src="/assets/js/jquery-modal-video.min.js"></script>
        <!-- slick js -->
        <script src="/assets/js/slick.min.js"></script>
        <!-- toastr js -->
        <script src="/assets/js/toastr.min.js"></script>
        <!-- chart js -->
        <script src="/assets/js/Chart.min.js"></script>
        <script src="/assets/js/chart-activate.js"></script>
        <!-- utils for chart js -->
        <script src="/assets/js/utils.js"></script>
        <!-- data table -->
        <script src="/assets/js/jquery.dataTables.js"></script>
        <script src="/assets/js/data-able-active.js"></script>
        <!-- clock js -->
        <script src="/assets/js/clock.min.js"></script>
        <!-- main -->
        <script src="/assets/js/main.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61df1ce6b84f7301d32ab604/1fp7ohq32';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>
