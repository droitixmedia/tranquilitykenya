<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from iamubaidah.com/html/oitila/live/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 May 2021 10:50:45 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Tranquility Shares | Forex Shares Portal</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="/assets/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="/assets/fonts/flaticon.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="/assets/css/animate.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="/assets/css/modal-video.min.css">
    <!-- slick css -->
    <link rel="stylesheet" href="/assets/css/slick.css">
    <link rel="stylesheet" href="/assets/css/slick-theme.css">
    <!-- toastr js -->
    <link rel="stylesheet" href="/assets/css/toastr.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

    <body>

        <div class="notification-alert">
            <div class="notice-list">
                
            </div>
        </div>

        <!-- preloader begin-->
        <div class="preloader">
            <img src="assets/img/tenor.gif" alt="">
        </div>
        <!-- preloader end -->

        <div class="mobile-navbar-wrapper">

           @include('layouts.partials.offlinenav')

            <!-- banner begin -->
            <div class="banner">
                <div class="container">
                    <div class="row justify-content-xl-between justify-content-lg-between justify-content-md-center justify-content-sm-center">
                        <div class="col-xl-7 col-lg-7 col-sm-10 col-md-9 d-xl-flex d-lg-flex d-block align-items-center d-banner-tamim">
                            <div class="banner-content">
                                <h4>Want to share forex profits with us?</h4>
                                <h1>Grow your money with safety invest in Tranquility Shares</h1>
                                <p>We are a group of seasoned and profitable forex traders, ready to let you join our profit share as we benefit from large volume forex trading that you make possible with your investments.</p>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox">Create investment plan now</a>
                            </div>
                            <div class="banner-statics">
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/svg/start.svg" alt="">
                                    </div>
                                    <div class="part-text">
                                        <span class="text">Choose plan from</span>
                                        <span class="number">10% Daily</span>
                                    </div>
                                </div>
                                <div class="single-statics">
                                    <div class="part-icon">
                                        <img src="assets/img/svg/profit.svg" alt="">
                                    </div>
                                    <div class="part-text">
                                        <span class="text">Minimum: Ksh1000</span>
                                        <span class="number">Maximun:Ksh1000000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-5 col-sm-10 col-md-8 monitor-for-480">
                            <div class="profit-calculator">
                                <div class="calc-header">
                                    <h3 class="title">Calculate <span class="special"> Your Profit</span></h3>
                                </div>
                                <div class="calc-body">
                                    <div class="part-plan">
                                        <h4 class="title">
                                            Choose Profit Share Plan
                                        </h4>
                                        <div class="dropdown show">
                                            <a class="dropdown-toggle displayed-selected-plan" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                               GOLD (10% Daily)
                                            </a>
                                            <div class="dropdown-menu plan-select-list" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item single-select-plan selected-plan active" href="#" data-max-amount="9999" data-min-amount="1000" data-package-no="1" data-parcentage="10" data-days="30">GOLD (10% Daily)</a>
                                                <a class="dropdown-item single-select-plan" href="#" data-package-no="2" data-max-amount="99999" data-min-amount="10000" data-parcentage="12" data-days="30">PLATINUM(12% Daily)</a>
                                                <a class="dropdown-item single-select-plan" href="#" data-package-no="3" data-max-amount="1000000" data-min-amount="100000" data-parcentage="15" data-days="30">DIAMOND (15% Daily)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="part-amount">
                                        <h4 class="title">
                                            Enter Example Amount
                                        </h4>
                                        <form>
                                            <span class="currency-symbol" id="basic-addon1">Ksh</span>
                                            <input type="text" class="inputted-amount" value="900">
                                                <button class="dropdown-toggle displayed-selected-currency" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ksh
                                                </button>
                                                <div class="dropdown-menu currency-select-list" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item single-currency-select selected-currency active" href="#" data-currency="pula">Ksh</a>
                                                    <a class="dropdown-item single-currency-select" href="#" data-currency="usd">USD</a>
                                                   
                                                </div> 
                                        </form>
                                    </div>
                                    <div class="d-inline-block cursor-not-allowed">
                                        <button class="calculate-all">Calculate</button>
                                    </div>
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="part-result">
                                    <ul>
                                        <li>
                                            <div class="icon">
                                                <img src="assets/img/svg/business-and-finance.svg" alt="">
                                            </div>
                                            <div class="text">
                                                <span class="title">Total<br/> Percent</span>
                                                <span class="number js_totalPercentage">10%</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="icon">
                                                <img src="assets/img/svg/profit.svg" alt="">
                                            </div>
                                            <div class="text">
                                                <span class="title">Daily<br/> Profits</span>
                                            <span class="number js_dailyProfit">Ksh90</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="icon">
                                                <img src="assets/img/svg/profits.svg" alt="">
                                            </div>
                                            <div class="text">
                                                <span class="title">Net<br/> Profit</span>
                                            <span class="number js_netProfit">Ksh 1800</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="icon">
                                                <img src="assets/img/svg/return-on-investment.svg" alt="">
                                            </div>
                                            <div class="text">
                                                <span class="title">Total<br/> Return</span>
                                                <span class="number js_totalReturn">Ksh 2700</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- banner end -->
 
            <!-- about begin -->
            <div class="about">
                <div class="container">
                    <div class="how-to-works">
                        <div class="row justify-content-center justify-content-sm-center justify-content-md-start">
                            <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6">
                                <div class="single-system">
                                    <div class="part-icon">
                                        <img src="assets/img/svg/add-user.svg" alt="">
                                    </div>
                                    <div class="part-text">
                                        <h4 class="title">Create Account</h4>
                                        <p>Create an account and upload your profile picture to get free Bonus.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6">
                                <div class="single-system">
                                    <div class="part-icon">
                                        <img src="assets/img/svg/coin.svg" alt="">
                                    </div>
                                    <div class="part-text">
                                        <h4 class="title">Invest Money</h4>
                                        <p>Select a profit sharing plan that you want and invest any amount from Ksh1000.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6">
                                <div class="single-system">
                                    <div class="part-icon">
                                        <img src="assets/img/svg/money-bag.svg" alt="">
                                    </div>
                                    <div class="part-text">
                                        <h4 class="title">Withdraw Your Money</h4>
                                        <p>Once your investment matures, click on withdraw and your money will be sent to you instantly from 9am to 5pm daily.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/" rel="noopener" target="_blank"><span class="blue-text">Financial Markets</span></a> by TradingView</div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js" async>
  {
  "width": 770,
  "height": 450,
  "symbolsGroups": [
    {
      "name": "Indices",
      "originalName": "Indices",
      "symbols": [
        {
          "name": "FOREXCOM:SPXUSD",
          "displayName": "S&P 500"
        },
        {
          "name": "FOREXCOM:NSXUSD",
          "displayName": "US 100"
        },
        {
          "name": "FOREXCOM:DJI",
          "displayName": "Dow 30"
        },
        {
          "name": "INDEX:NKY",
          "displayName": "Nikkei 225"
        },
        {
          "name": "INDEX:DEU40",
          "displayName": "DAX Index"
        },
        {
          "name": "FOREXCOM:UKXGBP",
          "displayName": "UK 100"
        }
      ]
    },
    {
      "name": "Futures",
      "originalName": "Futures",
      "symbols": [
        {
          "name": "CME_MINI:ES1!",
          "displayName": "S&P 500"
        },
        {
          "name": "CME:6E1!",
          "displayName": "Euro"
        },
        {
          "name": "COMEX:GC1!",
          "displayName": "Gold"
        },
        {
          "name": "NYMEX:CL1!",
          "displayName": "Crude Oil"
        },
        {
          "name": "NYMEX:NG1!",
          "displayName": "Natural Gas"
        },
        {
          "name": "CBOT:ZC1!",
          "displayName": "Corn"
        }
      ]
    },
    {
      "name": "Bonds",
      "originalName": "Bonds",
      "symbols": [
        {
          "name": "CME:GE1!",
          "displayName": "Eurodollar"
        },
        {
          "name": "CBOT:ZB1!",
          "displayName": "T-Bond"
        },
        {
          "name": "CBOT:UB1!",
          "displayName": "Ultra T-Bond"
        },
        {
          "name": "EUREX:FGBL1!",
          "displayName": "Euro Bund"
        },
        {
          "name": "EUREX:FBTP1!",
          "displayName": "Euro BTP"
        },
        {
          "name": "EUREX:FGBM1!",
          "displayName": "Euro BOBL"
        }
      ]
    },
    {
      "name": "Forex",
      "originalName": "Forex",
      "symbols": [
        {
          "name": "FX:EURUSD"
        },
        {
          "name": "FX:GBPUSD"
        },
        {
          "name": "FX:USDJPY"
        },
        {
          "name": "FX:USDCHF"
        },
        {
          "name": "FX:AUDUSD"
        },
        {
          "name": "FX:USDCAD"
        }
      ]
    }
  ],
  "showSymbolLogo": true,
  "colorTheme": "dark",
  "isTransparent": false,
  "locale": "en"
}
  </script>
</div>
                    <div class="row justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-center">
                        <div class="col-xl-6 col-lg-6 col-sm-10">
                            <div class="part-text">
                                <h2>Take <span class="special">advantage </span>of our years of forex trading experience.</h2>
                                <p>Once you create an investment order and its approved, your order will be added to its    <span class="spc">respective profit pool</span> and can be withdrawn instantly on maturity date.</p>
                                <ul>
                                    <li><i class="fas fa-check"></i> Withdrawals are every 11am of your maturity date. </li>
                                    <li><i class="fas fa-check"></i> Our withdrawals are approved instantly.  </li>
                                    <li><i class="fas fa-check"></i> Your capital + Your profit is withrawable at once. </li>
                                    <li><i class="fas fa-check"></i> You can upgrade your shares to a higher plan anytime.</li>
                                </ul>
                                <p>We are taking advantage of high volume forex trading and operate 5 large accounts with 3 reputable forex brokers.</p>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-2">Create Order Now</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-sm-10 col-md-12">
                            <div class="part-feature">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/solar-energy.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We Have an Elite Team</h3>
                                                <p>Our team of forex traders is well grounded and bags years of experience in the field </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/diploma.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We're Registered</h3>
                                                <p>Tranquility is a registered forex trading company that has physical working offices . </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/blockchain.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We Are Transparent</h3>
                                                <p>Every 5 Days we release our weekly perfomance statistics to our investors for any purpose they may deem usefull. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/worldwide.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We're Global</h3>
                                                <p>Tranquility is fast growing into a global forex profit sharing institution with operations in many countries</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- about end -->

            <!-- statics begin -->
            <div class="statics">
                <div class="container">
                    <div class="all-statics">
                        <div class="row no-gutters justify-content-center">
                            <div class="col-xl-4 col-lg-3 col-sm-10 col-md-4">
                                <div class="single-statics">
                                    <div class="part-img">
                                        <img src="assets/img/svg/investor.svg" alt="investor">
                                    </div>
                                    <div class="part-text">
                                       
                                        <span class="title">Smooth Deposits</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-3 col-sm-10 col-md-4">
                                <div class="single-statics">
                                    <div class="part-img">
                                        <img src="assets/img/svg/withdraw.svg" alt="investor">
                                    </div>
                                    <div class="part-text">
                                       
                                        <span class="title">Instant Withdrawal</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-3 col-sm-10 col-md-4">
                                <div class="single-statics">
                                    <div class="part-img">
                                        <img src="assets/img/svg/money-transfering.svg" alt="investor">
                                    </div>
                                    <div class="part-text">
                                       
                                        <span class="title">Transparent Investors</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- statics end -->

            <!-- prcing plan begin -->
            <div class="pricing-plan">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title">
                                <span class="sub-title">
                                    Join a Profit Pool
                                </span>
                                <h2>
                                    Tranquility<span class="special">Profit Pool</span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center justify-content-md-start">
                        <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6 prc-col">
                            <div class="single-plan">
                                <h3>Gold</h3>
                                <div class="plan-icon">
                                    <img src="assets/img/icon/bronze-medal.png" alt="">
                                </div>
                                <div class="feature-list">
                                    <ul>
                                        <li><i class="fas fa-check"></i> Minimum Deposit Ksh1000</li>
                                        <li><i class="fas fa-check"></i> Maximum Deposit Ksh9999</li>
                                        <li><i class="fas fa-check"></i> Daily</li>
                                        
                                    </ul>
                                </div>
                                <div class="price-info">
                                    <span class="parcent">10%</span>
                                   
                                </div>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-medium price-button">Invest now</a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6 prc-col">
                            <div class="single-plan active">
                                <h3>Platinum</h3>
                                <div class="plan-icon">
                                    <img src="assets/img/icon/trophy-1.png" alt="">
                                </div>
                                <div class="feature-list">
                                    <ul>
                                       <li><i class="fas fa-check"></i> Minimum Deposit Ksh10000</li>
                                        <li><i class="fas fa-check"></i> Maximum Deposit Ksh99999</li>
                                        <li><i class="fas fa-check"></i> Daily</li>
                                    </ul>
                                </div>
                                <div class="price-info">
                                    <span class="parcent">12%</span>
                                   
                                </div>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-medium price-button">Invest now</a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-10 col-md-6 prc-col">
                            <div class="single-plan">
                                <h3>Diamond</h3>
                                <div class="plan-icon">
                                    <img src="assets/img/icon/trophy.png" alt="">
                                </div>
                                <div class="feature-list">
                                    <ul>
                                       <li><i class="fas fa-check"></i> Minimum Deposit Ksh100000</li>
                                        <li><i class="fas fa-check"></i> Maximum Deposit Ksh10000000</li>
                                        <li><i class="fas fa-check"></i>Daily</li>
                                    </ul>
                                </div>
                                <div class="price-info">
                                    <span class="parcent">15%</span>
                                    
                                </div>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-medium price-button">Invest now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- prcing plan end -->

           
            <!-- transaction begin -->
            <div class="transaction">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title section-title-2">
                                <span class="sub-title">
                                    Checkout
                                </span>
                                <h2>
                                     recent transactions
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-center">
                        <div class="col-xl-5 col-lg-5 col-sm-10 col-md-12">
                            <div class="transaction-list">
                                <div class="Vertical-Slider">
                                   @if ($listings->count())
    @foreach ($listings as $listing)
            @include ('listings.partials._listing', compact('listing'))
    @endforeach



    @else
        <p>No Transactions Yet.</p>
    @endif
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-sm-10 col-md-12 d-xl-flex d-lg-flex d-block align-items-center">
                            <div class="part-text">
                                <h2>see all our system activity</h2>
                                <p>This is a summary of all recent deposits and withdrawals done on the system.</p>
                                <p class="marked"><b>Important:</b> All transactions will have been finalized at the time of display,no pending transactions are displayed.</p>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-medium cta-btn">Invest Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- transaction end -->

            <!-- testimonial begin -->
            <div class="testimonial">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title">
                                <span class="sub-title">
                                    Investors Feedback
                                </span>
                                <h2>
                                    Investors are<span class="special">  Satisfied</span>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="all-testimonials">
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8">
                                <div class="testi-text-slider">
                                    @if ($listings->count())
                                           @foreach ($listings as $listing)
                                           @if ($listing->comments->count())
                                         @foreach($listing->comments as $comment)
                                            <div class="single-testimonial">
                                        <span class="quot-icon">
                                            <img src="assets/img/icon/quot.png" alt="">
                                        </span>
                                        <p>{{$comment->body}}</p>
                                        <div class="part-user">
                                            <span class="user-name">{{$comment->listing->user->name}} {{$comment->listing->user->surname}}</span>
                                            <span class="user-location">Kenya</span>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else

                                    @endif
                                           @endforeach



                                           @else
                                                <p>No Testimonial Yet.</p>
                                    @endif
                           
                                    

                                   

                                </div>
                                <div class="testi-user-slider">
                                     @if ($listings->count())
                                           @foreach ($listings as $listing)
                                           @if ($listing->comments->count())
                                         @foreach($listing->comments as $comment)
                                    <div class="single-user">
                                        <img src="/uploads/avatars/{{ $comment->user->avatar }}" alt="">
                                    </div>
                                    @endforeach
                                    @else

                                    @endif
                                           @endforeach



                                           @else
                                                <p>No Testimonial Yet.</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- testimonial end -->

            <!-- payment gateway begin -->
            <div class="payment-gateway">
                <div class="container">
                    <div class="row justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-center">
                        <div class="col-xl-8 col-lg-8 col-sm-10 col-md-12 d-xl-flex d-lg-flex d-block align-items-center">
                          
                        </div>
                       
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12 col-sm-10 col-md-12">
                            <div class="all-payment">
                            <h3 class="title">Available Payment Methods :</h3>
                                <div class="gateway-slider">
                                    <div class="single-payment-way">
                                        <img src="assets/img/brand/kenya1.png" alt="">
                                    </div>
                                    <div class="single-payment-way">
                                        <img src="assets/img/brand/kenya2.png" alt="">
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- payment gateway end -->

          

            
                <div class="copyright-area" >
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-8">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer end -->
            
        </div> <!-- mobile navbar wrapper end -->

        
        <div class="d-xl-none d-lg-none d-block">
            <div class="mobile-navigation-bar">
             <ul>
                    <li>
                        <a href="{{url('profile')}}">
                            <img src="/assets/img/svg/profile.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('listings.create', [$area]) }}">
                            <img src="/assets/img/svg/money-transfering.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{{url('dashboard')}}">
                            <img src="/assets/img/svg/investor.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#header">
                            <img src="/assets/img/svg/arrow.svg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-xl-block d-lg-block d-none">
            <div class="back-to-top-btn">
                <a href="#">
                    <img src="assets/img/svg/arrow.svg" alt="">
                </a>
            </div>
        </div>
     
        <!-- jquery -->
        <script src="/assets/js/jquery.js"></script>
        <!-- popper js -->
        <script src="/assets/js/popper.min.js"></script>
        <!-- bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <!-- modal video js -->
        <script src="/assets/js/jquery-modal-video.min.js"></script>
        <!-- slick js -->
        <script src="/assets/js/slick.min.js"></script>
        <!-- toastr js -->
        <script src="/assets/js/toastr.min.js"></script>
        <!-- investment profit calculator-->
        <script src="/assets/js/investment-profit-calculator.js"></script>
        <!-- main -->
        <script src="/assets/js/main.js"></script>

    </body>


<!-- Mirrored from iamubaidah.com/html/oitila/live/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 May 2021 10:52:58 GMT -->
</html>