@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 


             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Downliners</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Downliners</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                                      <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="transactions-table">
                                <h3 class="title">
                                    Downliners
                                </h3>
                                <div class="table-responsive">
                                    <table id="example" class="display misco-data-table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact</th>
                                                <th>My Bonus</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($referrals as $ref)
                                            <tr>
                                                <td>{{$ref->name}} {{$ref->surname}}</td>
                                                <td>{{$ref->email}}</td>
                                                <td>{{$ref->phone}}</td>
                                                <td></td>
                                                
                                            </tr>
                                              @endforeach
                                           
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>     
                        
        


                   
                </div>
            </div>
            <!-- account end -->

@endsection
