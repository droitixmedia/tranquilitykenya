<?php

namespace openjobs;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use NodeTrait;

    protected $fillable = ['name', 'slug'];

public function getRouteKeyName()
    {
        return 'slug';
    }

 public function scopeWithListingsInArea($query, Area $area)
    {
        return $query->with(['listings' => function ($q) use ($area) {
            $q->isLive()->inArea($area);
        }]);
    }

   public function scopeWithResumesInArea($query, Area $area)
    {
        return $query->with(['resumes' => function ($q) use ($area) {
            $q->isLive()->inArea($area);
        }]);
    }

  public function scopeWithListings($query)
    {
        return $query->with(['listings' => function ($q) {
            $q->isLive();
        }]);
    }

  public function scopeWithResumes($query)
    {
        return $query->with(['resumes' => function ($q) {
            $q->isLive();
        }]);
    }

public function listings()
    {
        return $this->hasMany(Listing::class);
    }

 public function comments()
    {
        return $this->hasMany(Comment::class);
    }

 public function resumes()
    {
        return $this->hasMany(Resume::class);
    }

}
