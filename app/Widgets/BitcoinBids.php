<?php

namespace openjobs\Widgets;

use Arrilot\Widgets\AbstractWidget;
use openjobs\{Area, Category, Listing};
use Illuminate\Http\Request;
use Auth;



class BitcoinBids extends AbstractWidget
{

    public $reloadTimeout = 5;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */


    public function run(Area $area, Listing $listing)
    {

$listings = Listing::all()->where('live',true)->where('type', 1);

$now = \Carbon\Carbon::now();
$start = '12:00:00';
$end = '12:15:00';
$time = $now->format('H:i:s');



        return view('widgets.bitcoin_bids', compact('listings','start','end','time'));
    }
}
