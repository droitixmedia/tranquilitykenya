<?php

namespace openjobs\Http\Controllers;

use Auth;
use openjobs\Area;
use openjobs\Comment;
use openjobs\Listing;
use openjobs\Twilio\SendSmsNotification;
use openjobs\Category;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use openjobs\Approval;
use openjobs\Traits\Eloquent\{OrderableTrait, PivotOrderableTrait};


class HomeController extends Controller
{
     
    const INDEX_LIMIT = 10;
    public function dashboard(Area $area, Category $category, Listing $listing, Comment $comment)
    {
        $listings = Listing::all()->where('live', true);

        $unpublishedcount = Auth::user()->listings()->where('live', false)->count();


    $referralink = 'https://tranquilityforex.info/register/?ref=' . auth()->user()->id;



        return view('dashboard', compact('listings', 'unpublishedcount', 'referralink'));
    }

     

    public function index(Area $area, Category $category, Listing $listing)
    {
          $listings = Listing::all()->where('live', true);

        


        return view('home', compact('listings'));
    }
    
    public function referral()
    {

    }

    public function referrer()
    {
        return auth()->user()->referrer;
    }

    public function referrals()
    {
        $referrals = auth()->user()->referrals()->paginate(20);

        return view('referrals', compact('referrals'));
    }

    public function notifications()
    {
        return view('notifications');
    }

    public function markNotificationAsRead(DatabaseNotification $notification)
    {
        $notification->markAsRead();

        return response()->json([], 200);
    }
}
