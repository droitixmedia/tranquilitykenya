<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'body'=>'required',
        ]);

        $input = $request->all();
        $input['body'] = $listing->amount*($listing->category->parent->percent);
        $input['user_id'] = auth()->user()->id;

        Comment::create($input);

          return redirect()->back()->withSuccess('Bid successfully placed!!!!');
    }
}
