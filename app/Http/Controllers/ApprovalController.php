<?php

namespace openjobs\Http\Controllers;

use Auth;
use Session;
use openjobs\Area;
use openjobs\Comment;
use openjobs\Approval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use openjobs\Notifications\PaymentApprovedNotification;

class ApprovalController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, $comment_id)
    {
        $comment = Comment::find($comment_id);




        $approval = new Approval();
        $approval->body = $request->body;

        $approval->user_id = auth()->user()->id;

        $approval->comment()->associate($comment);
        $approval->save();


        Notification::send($request->user(), new PaymentApprovedNotification($approval));

        return back()->withSuccess('Approved');
    }




    public function destroy(Area $area, Approval $approval)
    {
        $this->authorize('destroy', $approval);

        $approval->delete();

        return back()->withSuccess('Approval deleted.');
    }
}
