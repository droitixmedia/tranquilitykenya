<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\{Area, Category, Listing,Comment};
use openjobs\Http\Controllers\Controller;
use Auth;

class CommentPublishedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user=Auth::user();
        $comments = $request->user()->comments()->paginate(10);



        return view('user.comments.published.index', compact('comments','user'));
    }


}

