<?php

namespace openjobs\Http\Controllers\Resume;

use openjobs\{Area, Resume};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;

class ResumePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function show(Area $area, Resume $resume)
    {
        $this->authorize('touch', $resume);

        if ($resume->live()) {
            return back();
        }

        return view('resumes.payment.show', compact('resume'));
    }

    public function store(Request $request, Area $area, Resume $resume)
    {
        $this->authorize('touch', $resume);

        if ($resume->live()) {
            return back();
        }

        if ($resume->cost() === 0) {
            return back();
        }

        if (($nonce = $request->payment_method_nonce) === null) {
            return back();
        }

        $result = \Braintree_Transaction::sale([
            'amount' => $resume->cost(),
            'paymentMethodNonce' => $nonce,
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        if ($result->success === false) {
            return back()->withError('Something went wrong while processing your payment.');
        }

        $resume->live = true;
        $resume->created_at = \Carbon\Carbon::now();
        $resume->save();

        return redirect()
            ->route('resume.show', [$area, $resume])
            ->withSuccess('Congratulations! Payment accepted and your listing is live.');
    }

    public function update(Request $request, Area $area, Resume $resume)
    {
        $this->authorize('touch', $resume);

        if ($resume->cost() > 0) {
            return back();
        }

        $resume->live = true;
        $resume->created_at = \Carbon\Carbon::now();
        $resume->save();

        return redirect()
            ->route('resumes.show', [$area, $resume])
            ->withSuccess('Congratulations! Your listing is live.');
    }
}
