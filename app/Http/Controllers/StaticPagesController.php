<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;

class StaticPagesController extends Controller

{

     public function companyreg(){
        return view('auth.companyreg');
    }

       public function fx(){
        return view('pages.fx');
    }

      public function pay(){
        return view('pay');
    }

      public function taken(){
        return view('taken');
    }
    public function about(){
        return view('pages.about');
    }

     public function cvwriting(){
        return view('pages.cvwriting');
    }
    public function bookacv(){
        return view('pages.bookacv');
    }
    public function terms(){
        return view('pages.terms');
    }

    public function privacy(){
        return view('pages.privacy');
    }

     public function faq(){
        return view('pages.faq');
    }

     public function test(){
        return view('pages.test');
    }




    public function contact(){
        return view('pages.contact');
    }



}
