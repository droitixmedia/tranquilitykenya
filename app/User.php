<?php

namespace openjobs;

use openjobs\Permissions\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


   class User extends Authenticatable implements MustVerifyEmail
   {



    use Notifiable,HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname','phone_number','bank','account','btcaddress','accounttype','email','referred_by', 'password','phone_number', 'isVerified',
    ];

     protected $dispatchesEvents = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     public function scopeIsCompany($query)
    {
        return $query->where('truecompany', true);
    }


     public function scopeIsNotCompany($query)
    {
        return $query->where('truecompany', false);
    }





   public function favouriteListings()
    {
        return $this->morphedByMany(Listing::class, 'favouriteable')
            ->withPivot(['created_at'])
            ->orderByPivot('created_at', 'desc');

    }

    public function favouriteResumes()
    {
        return $this->morphedByMany(Resume::class, 'favouriteable')
            ->withPivot(['created_at'])
            ->orderByPivot('created_at', 'desc');

    }

    public function viewedListings()
    {
        return $this->belongsToMany(Listing::class, 'user_listing_views')->withTimestamps()->withPivot(['count', 'id']);
    }

     public function viewedResumes()
    {
        return $this->belongsToMany(Resume::class, 'user_resume_views')->withTimestamps()->withPivot(['count', 'id']);
    }

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }

         public function approvals()
    {
        return $this->hasMany(Approval::class);
    }

     public function comments()
    {
        return $this->hasMany(Comment::class);
    }

      public function files()
    {
        return $this->hasMany(File::class);
    }

    public function resumes()
    {
        return $this->hasMany(Resume::class);
    }

    public function truecompany()
    {
        return $this->truecompany;
    }

    public function referrer()
  {
    return $this->belongsTo('openjobs\User', 'referred_by');
  }

public function referrals()
{
    return $this->hasMany('openjobs\User', 'referred_by');
}

}

