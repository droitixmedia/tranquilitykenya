<?php

namespace openjobs\Policies;

use openjobs\{User, Resume};
use Illuminate\Auth\Access\HandlesAuthorization;

class ResumePolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Resume $resume)
    {
        return $this->touch($user, $resume);
    }

    public function update(User $user, Resume $resume)
    {
        return $this->touch($user, $resume);
    }

    public function destroy(User $user, Resume $resume)
    {
        return $this->touch($user, $resume);
    }

    public function touch(User $user, Resume $resume)
    {
        return $resume->ownedByUser($user);
    }
}
