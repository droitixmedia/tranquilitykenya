<?php

namespace openjobs\Mail;

use openjobs\{Resume, User};
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListingShared extends Mailable
{
    use Queueable, SerializesModels;

    public $resume;

    public $sender;

    public $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Listing $listing, User $sender, $body = null)
    {
        $this->resume = $resume;
        $this->sender = $sender;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('email.resume.shared.message')
            ->subject("{$this->sender->name} shared a listing with you.")
            ->from('hello@fresh.com');
    }
}
