<?php

namespace openjobs\Twilio;

use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;

class SendSmsNotification
{
    public function sendSms($sendTo, string $message)
    {
        try {
            $client =  new Client(env("TWILIO_SID"), env("TWILIO_AUTH_TOKEN"));
            $message = $client->messages->create($sendTo, [
                'from' => env("TWILIO_FROM_NUMBER"),
                'body' => $message
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
        }
    }
}
