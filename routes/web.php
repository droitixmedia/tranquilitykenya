<?php
  Auth::routes();

               


Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::get('/notifications', 'HomeController@notifications')->name('notifications');
Route::post('notifications/{notification}/mark-as-read', 'HomeController@markNotificationAsRead')->name('notifications.mark_as_read');

                  Route::get('/', 'HomeController@index');
                  Route::get('/fx', 'StaticPagesController@fx');

        Route::group(['middleware' => 'auth'], function () {
            Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');


            Route::get('/bidding', 'BiddingRoomController@index');
            Route::get('/bitcoin', 'BitcoinRoomController@index');
            Route::get('/widget', 'WidgetController@index');

            Route::get('/taken', 'StaticPagesController@taken');

            Route::get('/sell', 'CommentController@sell')->name('comments.sell');
        });
Route::get('/cookie', function () {
    return Cookie::get('referral');
});
Route::get('/referral-link', 'HomeController@referral');

Route::get('/referrer', 'HomeController@referrer');
Route::get('/referrals', 'HomeController@referrals');

Route::post('/uploaddocs', 'UploadController@upload');
 Route::delete('/{file}', 'UploadController@destroy')->name('files.destroy');

         Route::get('/user/{user_id}', 'UserController@getProfile')->name('profile.index');

Route::group(['prefix' => '/{area}'], function () {
    Route::get('/jobs-inarea', 'Listing\ListingController@inarea')->name('listings.inarea');
});

Route::group(['prefix' => '/{category}'], function () {
    Route::get('/jobs', 'Listing\ListingController@index')->name('listings.index');
});


Route::group(['prefix' => '/{area}'], function () {
    Route::get('/all-jobs-in-south-africa', 'Listing\ListingController@allsa')->name('listings.allsa');
});



Route::group(['prefix' => '/{category}'], function () {
    Route::get('/resumes', 'Resume\ResumeController@index')->name('resumes.index');
});

              Route::post('/comments/{listing_id}', 'CommentController@store')->middleware('auth')->name('comments.store');

              Route::post('/approval/{comment_id}', 'ApprovalController@store')->middleware('auth')->name('approvals.store');



              Route::get('/my-bids', 'CommentPublishedController@index')->middleware('auth')->name('comments.published.index');


            Route::get('/files', 'FilePublishedController@index')->middleware('auth')->name('files.published.index');
            Route::get('/uploaddocs', 'UploadController@index')->middleware('auth')->name('files.upload.index');




               
    Route::delete('/admin/impersonate', 'Admin\ImpersonateController@destroy');
Route::group(['middleware' => 'role:admin'], function () {
         Route::get('admin/investments', 'Admin\ListingController@invested');
          Route::get('admin/withdrawals', 'Admin\ListingController@withdrawal');
    Route::get('admin/users', 'Admin\UserController@index');
    Route::get('admin/plans', 'Admin\PlanController@index');
    Route::get('admin/listings', 'Admin\ListingController@display');



    Route::get('/admin/impersonate', 'Admin\ImpersonateController@index')->name('admin.impersonate');
    Route::post('/admin/impersonate', 'Admin\ImpersonateController@store');
    Route::delete('/users/{user}', 'Admin\UserController@destroyUser')->name('admin.user.destroy');
    Route::delete('/listings/{listing}', 'Admin\ListingController@destroyListing')->name('admin.listing.destroy');
    Route::delete('/comments/{comment}', 'Admin\CommentController@destroyComment')->name('admin.comment.destroy');
    Route::patch('/{listing}/update', 'Admin\ListingController@update')->name('admin.listing.update');

});
                 Route::post('/upload-image/{userId}', 'ImagesController@store');

                 Route::get('/user/area/{area}', 'User\AreaController@store')->name('user.area.store');

                 Route::get('/braintree/token', 'BraintreeController@token');

                 Route::get('profile', 'UserController@profile')->name('profile')->middleware('auth');

                 Route::get('/search', 'Listing\ListingController@search')->name('search');



                 Route::post('/profile/update-avatar', 'UserController@update_avatar')->middleware('auth')->name('profile.update.avatar');
                 Route::post('/profile', 'UserController@update')->middleware('auth')->name('profile.update');
Route::group(['prefix' => '/{area}'], function () {
    Route::delete('/{comment}', 'CommentController@destroy')->middleware('auth')->name('comment.destroy');

    /**
     * Category
     */
    Route::group(['prefix' => '/categories'], function () {
        Route::get('/', 'Category\CategoryController@index')->name('category.index');
    });


    /**
     * Listings
     */
    Route::group(['prefix' => '/listings', 'namespace' => 'Listing'], function () {
        Route::get('/same-category/{category}', 'ListingPublishedController@listingsInCategory')->name('listings.published.index');

        Route::get('/favourites', 'ListingFavouriteController@index')->name('listings.favourites.index');

        Route::post('/{listing}/favourites', 'ListingFavouriteController@store')->name('listings.favourites.store');
        Route::delete('/{listing}/favourites', 'ListingFavouriteController@destroy')->name('listings.favourites.destroy');

        Route::get('/viewed', 'ListingViewedController@index')->name('listings.viewed.index')->middleware('auth');


        Route::post('/{listing}/contact', 'ListingContactController@store')->name('listings.contact.store');

        Route::get('/{listing}/payment', 'ListingPaymentController@show')->name('listings.payment.show');
        Route::post('/{listing}/payment', 'ListingPaymentController@store')->name('listings.payment.store');
        Route::patch('/{listing}/payment', 'ListingPaymentController@update')->name('listings.payment.update');

        Route::get('/unpublished', 'ListingUnpublishedController@index')->name('listings.unpublished.index');

        Route::get('/history', 'ListingHistoryController@index')->name('listings.history.index');


        Route::get('/published', 'ListingPublishedController@index')->name('listings.published.index');


        Route::get('/{listing}/share', 'ListingShareController@index')->name('listings.share.index');
        Route::post('/{listing}/share', 'ListingShareController@store')->name('listings.share.store');

        Route::group(['middleware' => 'auth'], function () {
            Route::get('/create', 'ListingController@create')->name('listings.create');
            Route::get('/bcreate', 'ListingController@bcreate')->name('listings.bcreate');

            Route::get('/bitcoincreate', 'ListingController@bitcoincreate')->name('listings.bitcoincreate');

            Route::get('/btcreate', 'ListingController@btcreate')->name('listings.btcreate');
            Route::post('/store', 'ListingController@store')->name('listings.store');


            Route::post('/{listingId}/reply', 'ListingController@postReply')->name('listings.reply');
            
            
            Route::get('/{listing}/invalidate', 'ListingController@invalidate')->name('listings.invalidate');
            Route::patch('/{listing}/close', 'ListingController@close')->name('listings.close');

            Route::get('/{listing}/edit', 'ListingController@edit')->name('listings.edit');
            Route::patch('/{listing}/update', 'ListingController@update')->name('listings.update');
            Route::patch('/{listing}', 'ListingController@disable')->name('listings.disable');
            Route::delete('/{listing}', 'ListingController@destroy')->name('listings.destroy');
        });
    });

    /**
    * Resumes
    */
    Route::group(['prefix' => '/resume', 'namespace' => 'Resume'], function () {
        Route::get('/same-category/{category}', 'ResumePublishedController@resumesInCategory')->name('resume.published.index');



        Route::get('/viewed', 'ResumeViewedController@index')->name('resumes.viewed.index')->middleware('auth');


        Route::post('/{resume}/contact', 'ResumeContactController@store')->name('resumes.contact.store');

        Route::get('/{resume}/payment', 'ResumePaymentController@show')->name('resumes.payment.show');
        Route::post('/{resume}/payment', 'ResumePaymentController@store')->name('resumes.payment.store');
        Route::patch('/{resume}/payment', 'ResumePaymentController@update')->name('resumes.payment.update');

        Route::get('/unpublished', 'ResumeUnpublishedController@index')->name('resumes.unpublished.index');


        Route::get('/published', 'ResumePublishedController@index')->name('resumes.published.index');


        Route::get('/{resume}/share', 'ResumeShareController@index')->name('resumes.share.index');
        Route::post('/{resume}/share', 'ResumeShareController@store')->name('resumes.share.store');

        Route::group(['middleware' => 'auth'], function () {
            Route::get('/create', 'ResumeController@create')->name('resumes.create');
            Route::post('/', 'ResumeController@store')->name('resumes.store');

            Route::get('/{resume}/edit', 'ResumeController@edit')->name('resumes.edit');
            Route::get('/{resume}/educationedit', 'ResumeController@eduedit')->name('resumes.eduedit');
            Route::get('/{resume}/experienceedit', 'ResumeController@compedit')->name('resumes.compedit');
            Route::patch('/{resume}', 'ResumeController@update')->name('resumes.update');
            Route::delete('/{resume}', 'ResumeController@destroy')->name('resumes.destroy');
        });
    });


    Route::get('/{resume}-resume-preview', 'Resume\ResumeController@show')->name('resumes.show');
    Route::get('/{resume}/candidate', 'Resume\ResumeController@reveal')->name('resumes.reveal');
    Route::get('/browse-jobs/{listing}-{slug?}', 'Listing\ListingController@show')->name('listings.show');

    Route::get('/bid/{comment}-details', 'CommentController@show')->name('comments.show');
    Route::get('/bid/{comment}-bitcoin', 'CommentController@bitshow')->name('comments.bitshow');


    Route::get('/apply/{listing}', 'Listing\ListingController@apply')->name('listings.apply');
    Route::get('/bid/{listing}', 'Listing\ListingController@bid')->name('listings.bid');

    Route::get('/applicants/{listing}', 'Listing\ListingController@applicants')->name('listings.applicants');
});





Route::get('/companyreg', 'StaticPagesController@companyreg');
Route::get('/about', 'StaticPagesController@about');
Route::get('/pay', 'StaticPagesController@pay');
Route::get('/test', 'StaticPagesController@test');
Route::get('/bookacv', 'StaticPagesController@bookacv');
Route::get('/cv-writing', 'StaticPagesController@cvwriting');
Route::get('/faq', 'StaticPagesController@faq');
Route::get('/privacy', 'StaticPagesController@privacy');
Route::get('/terms', 'StaticPagesController@terms');
Route::get('/contact', 'StaticPagesController@contact')->name('Pages.contact');
Route::post('/contact', 'StaticPagesController@store')->name('contact.send');
